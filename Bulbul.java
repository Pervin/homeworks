public class Bulbul extends Bird {
    public Bulbul(int age, String name) {
        super(age, name, "blue");
    }
}
