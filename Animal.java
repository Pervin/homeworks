public abstract class Animal {
    int age;
    String name;
    String color;
    public Animal(int age, String name, String color) {
        this.age = age;
        this.name = name;
        this.color=color;
    }

    public void getAge() {
        System.out.println(age);
    }

    public void getName() {
        System.out.println(name);
    }
    public void eat() {
        System.out.println("Yemek yeyir");
    }
    public abstract void sound();



    public void play() {
        System.out.println("Oynayir");
    }
}
