class Cat extends Animal {
    public Cat(int age, String name, String color) {
        super (age,name,color);
    }
    @Override
    public void sound() {
        System.out.println("MEOOOWWW");
    }

    public void eat() {
        System.out.println("NUM NUM ");
    }

    public void jumpy() {
        System.out.println("HOPP ");
    }
    public void getColor(){
        System.out.println(color);
    }
}
