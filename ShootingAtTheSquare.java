import java.util.Random;
import java.util.Scanner;

public class ShootingAtTheSquare {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        String[][] gameBoard = new String[5][5];

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                gameBoard[i][j] = "-";
            }
        }

        System.out.println("All set. Get ready to rumble!");

        int targetLine = random.nextInt(5);
        int targetBar = random.nextInt(5);
        gameBoard[targetLine][targetBar] = "x";


        while (true) {

            displayBoard(gameBoard);

            int line = getUserInput(scanner, "Enter a line (1-5):") - 1;

            int bar = getUserInput(scanner, "Enter a bar (1-5):") - 1;

            if (line < 0 || line >= 5 || bar < 0 || bar >= 5) {
                System.out.println("Invalid input. Try again.");
                continue;
            }


            if (gameBoard[line][bar].equals("*")) {
                System.out.println("You already shot there. Try a different position.");
                continue;
            }


            gameBoard[line][bar] = "*";


            if (line == targetLine && bar == targetBar) {
                System.out.println("Congratulations! You have won!");
                displayBoard(gameBoard);
                break;
            }
        }

        scanner.close();
    }


    private static int getUserInput(Scanner scanner, String prompt) {
        int userInput;
        while (true) {
            System.out.print(prompt);
            if (scanner.hasNextInt()) {
                userInput = scanner.nextInt();
                if (userInput >= 1 && userInput <= 5) {
                    return userInput;
                } else {
                    System.out.println("Please enter a number between 1 and 5.");
                }
            } else {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.next();
            }
        }
    }


    private static void displayBoard(String[][] gameBoard) {
        System.out.println("  1   2   3   4   5");
        for (int i = 0; i < 5; i++) {
            System.out.print((i + 1) + " ");
            for (int j = 0; j < 5; j++) {
                System.out.print(gameBoard[i][j] + " ");
            }
            System.out.println();
        }
    }
}
