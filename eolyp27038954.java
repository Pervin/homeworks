import java.util.Scanner;
public class eolyp27038954 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] numbers = new int[n];

        // Massivi doldur
        for(int i = 0; i < n; i++){
            numbers[i] = in.nextInt();
        }

        // Massivi tərsinə çevir
        reverseArray(numbers);

        // Tərsinə çevrilmiş massivi çap et
        for(int i = 0; i < n; i++){
            System.out.print(numbers[i] + " ");
        }
    }

    // Massivi tərsinə çevirən funksiya
    public static void reverseArray(int[] arr) {
        int start = 0;
        int end = arr.length - 1;

        while (start < end) {
            // Mübadilə edirik
            int temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;

            // Indeksləri dəyişdir
            start++;

            end--;
        }
    }
}

