import java.util.Scanner;

public class WeekPlanner {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String[][] schedule = new String[7][2];


        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to gym";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "study programming";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "read a book";
        schedule[5][0] = "Friday";
        schedule[5][1] = "watch a series";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "relax and enjoy";


        while (true) {
            System.out.print("Please, input the day of the week: ");
            String inputDay = scanner.nextLine().trim().toLowerCase();

            if (inputDay.equals("exit")) {
                System.out.println("Exiting the program...");
                break; // Dövr bitir
            }


            boolean validDay = false;
            String task = "";

            for (int i = 0; i < 7; i++) {
                if (inputDay.equals(schedule[i][0].toLowerCase())) {
                    task = schedule[i][1];
                    validDay = true;
                    System.out.println("Your tasks for " + schedule[i][0] + ": " + task);
                    break;
                }
            }


            if (!validDay) {
                if (inputDay.startsWith("change") || inputDay.startsWith("reschedule")) {
                    String day = inputDay.split(" ")[1]; // "change Monday" kimi ifadə daxil edildikdə ikinci sözü götürürük
                    boolean found = false;

                    for (int i = 0; i < 7; i++) {
                        if (day.equals(schedule[i][0].toLowerCase())) {
                            System.out.print("Please, input new tasks for " + schedule[i][0] + ": ");
                            String newTasks = scanner.nextLine().trim();
                            schedule[i][1] = newTasks; // Yeni tapşırığı müvafiq yerə qeyd edirik
                            System.out.println("New tasks for " + schedule[i][0] + " have been updated.");
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        System.out.println("Sorry, I don't understand you, please try again.");
                    }
                } else {
                    System.out.println("Sorry, I don't understand you, please try again.");
                }
            }
        }

        scanner.close();
    }
}
