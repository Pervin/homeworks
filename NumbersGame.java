import java.util.Random;
import java.util.Scanner;

public class NumbersGame {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Let the game begin!");

        int secretNumber = random.nextInt(101);
        String name;

        System.out.print("Enter your name: ");
        name = scanner.nextLine();

        int[] enteredNumbers = new int[10];
        int count = 0;
        while (true) {
            System.out.print("Enter a number between 0 and 100: ");
            int userInput;

            while (!scanner.hasNextInt()) {
                System.out.println("Invalid input. Please enter a valid number.");
                scanner.next();
            }

            userInput = scanner.nextInt();

            if (userInput < 0 || userInput > 100) {
                System.out.println("Please enter a number between 0 and 100.");
                continue;
            }

            if (count == enteredNumbers.length) {
                int[] newEnteredNumbers = new int[enteredNumbers.length * 2];
                System.arraycopy(enteredNumbers, 0, newEnteredNumbers, 0, enteredNumbers.length);
                enteredNumbers = newEnteredNumbers;
            }
            enteredNumbers[count] = userInput;
            count++;

            if (userInput == secretNumber) {
                System.out.println("Congratulations, " + name + "!");
                break;
            } else if (userInput < secretNumber) {
                System.out.println("Your number is too small. Please, try again..");
            } else {
                System.out.println("Your number is too big. Please, try again..");
            }
        }

        System.out.println("Your numbers:");

        sortArrayDescending(enteredNumbers, count);

        for (int i = 0; i < count; i++) {
            System.out.print(enteredNumbers[i] + " ");
        }
    }

    private static void sortArrayDescending(int[] arr, int count) {
        // Bubble sort istifadə edərək sıralama
        for (int i = 0; i < count - 1; i++) {
            for (int j = 0; j < count - i - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
}
